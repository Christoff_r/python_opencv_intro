import numpy as np
import cv2 as cv
import time
import random

cap = cv.VideoCapture(0)
if not cap.isOpened():
    print("Cannot open camera")
    exit()

start_time = time.time()
font = cv.FONT_HERSHEY_COMPLEX
logo = cv.imread('logo.png')
fourcc = cv.VideoWriter_fourcc(*'DIVX')
out = cv.VideoWriter('output.avi', fourcc, 20.0, (640,480))

while True:
    ret, frame = cap.read()

    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break

    time_pased = time.time() - start_time

    # For the first 10 sec
    if(time_pased < 10 ):
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        frame[:,:,0] = gray
        frame[:,:,1] = gray
        frame[:,:,2] = gray
    
    # For the next 10 sec
    if (time_pased > 10 and time_pased < 20): 
        frame = cv.putText(frame, "Text :D", (10, 400), font, 4, (255,255,255), 2, cv.LINE_AA)

    # For the next 10 sec
    if (time_pased > 20 and time_pased < 30):
        logo = cv.resize(logo, (frame.shape[1], frame.shape[0]))
        frame = cv.addWeighted(frame, 0.7, logo, 0.3, 0)
    
    # For the next 10 sec
    if (time_pased > 30 and time_pased < 40):
        crop = frame[200:300, 250:350]
        frame[0:100, 50:150] = crop

    # For the next 10 sec
    if (time_pased > 40 and time_pased < 50):
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        
        # Define lower and uppper limits of what we call "blue"
        low = np.array([100,50,50])
        high = np.array([130,225,225])

        # Mask image to only select blue
        mask = cv.inRange(hsv, low, high)

        # Change image to red where we found blue
        frame[mask > 0 ] = (0,0,255)

    # For the next 10 sec
    if (time_pased > 60):
        break


    # Write frame
    out.write(frame)
    # Show frame
    cv.imshow('frame', frame)
    
    if cv.waitKey(1) == ord('q'):
        break

cap.release()
out.release()
cv.destroyAllWindows()